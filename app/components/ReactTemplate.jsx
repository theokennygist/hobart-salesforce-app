import React from 'react'

export default class Reacttemplate extends React.Component {
	render() {
		return (
			<div class="container">
				<section class="container text-center body-section">
					<h1 class="blue-color-heading">Heroku-React</h1>
					<h4>To get started, add jsx files into <code>app/components</code> folder</h4>
					<a class="btn btn-primary" href="http://heroku-react-doc.herokuapp.com">Learn more</a>
				</section>				
			</div>
		)
	}
}