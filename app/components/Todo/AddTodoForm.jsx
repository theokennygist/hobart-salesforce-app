import React from 'react'
var {connect}  = require('react-redux');
var actions = require('actions');


export class AddTodoForm extends React.Component {
	
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		var {dispatch} = this.props;
		let todoVal = this.refs.addTodoFields.value;

		if(todoVal.length > 0) {

			this.refs.addTodoFields.value = '';
			//this.props.addTodo(todoVal); //pass todoVal as props to parent
			dispatch(actions.addTodo(todoVal));
		}
		else {
			//put cursor back to input text
			this.refs.addTodoFields.focus();
		}		
	}
	
	render() {
		return (
			<div class="container__header">
				<form class="form-inline" onSubmit={this.handleSubmit}>
					<input type="text" class="form-control add-todo" placeholder="Add todo" ref="addTodoFields" />
					<button class="btn btn-primary">Add</button>
				</form>
			</div>
		)
	}
}

export default connect()(AddTodoForm)