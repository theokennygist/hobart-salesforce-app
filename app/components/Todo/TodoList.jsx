import React from 'react'
import TodoObj from 'TodoObj'

var {connect} = require('react-redux');
var TodoApi = require('TodoApi');

export class TodoList extends React.Component {
	render() {
		let {todos, showCompleted, searchText} = this.props;
		//renderTodos() - iterate {todos} obj which value from props
		//2 returns below are required otherwise it doesn't show the result
		let renderTodos = () => {
			
			if(todos.length === 0) {
				return (
					<p class="container__message">Nothing to do</p>
				);
			}

			return TodoApi.filterTodos(todos, showCompleted, searchText).map((todo) => {
				return (
					<TodoObj key={todo.id} {...todo} />
				)
				
			});
		}
		//{renderTodos()} - call renderTodos() with React syntax
		return (
			<div>
				{renderTodos()} 				
			</div>
		)
	}
}

export default connect(
	(state) => {
		return state
	}
)(TodoList)