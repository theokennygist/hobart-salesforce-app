import React from 'react'
import uuid from 'node-uuid'
import moment from 'moment'

import TodoList from 'TodoList'
import AddTodoForm from 'AddTodoForm'
import TodoSearch from 'TodoSearch'


export default class TodoApp extends React.Component {
	constructor(props) {
		super(props);		
	} //constructor

	render() {

		return (
			<div class="container">
			<h1 class="page-title">Todo</h1>
				<div class="row">
						<div class ="col-md-4 col-md-offset-4 appContainer">								
							<AddTodoForm/> <br />
							<TodoList/><br />
							<TodoSearch />
						</div>
					</div>
				</div>
			
		)
	}
}