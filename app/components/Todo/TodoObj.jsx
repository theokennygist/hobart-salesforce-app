import React from 'react'
import moment from 'moment'

var {connect} = require('react-redux');
var actions = require('actions');

export class TodoObj extends React.Component {

	render() {
		let {id, text, completed, createdAt, completedAt, dispatch} = this.props;
		var todoClassName = completed ? 'todo todo-completed' : 'todo';

		var renderDate = () => {
			var message = 'Created ';
			var timestamp = createdAt;
			
			if(completed) {
				message = 'Completed ';
				timestamp = completedAt;
			}

			//format the timestamp to be dd/month/year @time
			return message+ moment.unix(timestamp).format('Do MMM YYYY @ h:mma')
		};

		return (
			<div class={todoClassName} onClick={() => {
				dispatch(actions.toggleTodo(id));
			}}>
				<div>
					<input type="checkbox" checked={completed}/>
				</div>
				<div>
					<p>{text}</p>
					<p class="todo__subtext">{renderDate()}</p>
				</div>
			</div>
		)
	}
}

export default connect()(TodoObj);