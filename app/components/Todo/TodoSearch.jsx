import React from 'react'
var {connect} = require('react-redux');
var actions = require('actions');

export class TodoSearch extends React.Component {
	
	render() {
		var {dispatch, showCompleted, searchText} = this.props;

		return (
			<div class="container__footer">				
				<div class="form">
					<input type="text" class="form-control" ref="searchText" value={searchText} placeholder="Search todo" onChange={() => {
						var searchText = this.refs.searchText.value;
						dispatch(actions.setSearchText(searchText));
					}}/><br />			
				</div>
				<label>
					<input type="checkbox" ref="showCompleted" checked={showCompleted} onChange={() => {
						dispatch(actions.toggleShowCompleted());
					}}/>
						Show Completed
				</label>
			</div>
		)
	}
}

export default connect(
	(state) => {
		return {
			showCompleted: state.showCompleted,
			searchText: state.searchText
		}
	}
)(TodoSearch);