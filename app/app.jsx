import React from 'react'
import ReactDOM from 'react-dom'
import Bootstrap from 'bootstrap.css'
import Index from 'index.scss'
import {Route, Router, IndexRoute, hashHistory} from 'react-router'

var {Provider} = require('react-redux');

import Main from 'Main'
import Links from 'Links'
import TodoApp from 'TodoApp'

var TodoApi = require('TodoApi');

var actions = require('actions');
var store = require('configureStore').configure();


store.subscribe(() => {
	var state = store.getState();
	console.log('New state', state);
	TodoApi.setTodos(state.todos);
});

var initialTodos = TodoApi.getTodos();
store.dispatch(actions.addTodos(initialTodos));

export default class App extends React.Component {
	render() {
		return (
			<div class="container">
				<Main />
			</div>
		)
	}
}

const appDOM = document.getElementById('app')
//render() - a function from ReactDOM package
ReactDOM.render(
		<Provider store={store}>
			<Router history={hashHistory}>
				<Route path="/" component={Main}>
					<Route path="/links" component={Links} />
					<IndexRoute component={TodoApp} />
				</Route>
			</Router>
		</Provider>, 
			appDOM
);