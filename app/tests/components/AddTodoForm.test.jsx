import React from 'react'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'
import expect from 'expect'
import $ from 'jquery'

import {AddTodoForm} from 'AddTodoForm'

describe('ADDTODOFORM OPERATIONS', () => {
	it('AddTodoForm component should exist ', () => {
		expect(AddTodoForm).toExist();
	});

	it('should dispatch addTodo when put valid todo text', () => {
		var todoText = 'check email';
		var action = {
			type: 'ADD_TODO',
			text: todoText
		};
		var spy = expect.createSpy();
		var addTodo = TestUtils.renderIntoDocument(<AddTodoForm dispatch={spy}/>);
		var $el = $(ReactDOM.findDOMNode(addTodo));

		addTodo.refs.addTodoFields.value = todoText;
		TestUtils.Simulate.submit($el.find('form')[0]);

		expect(spy).toHaveBeenCalledWith(action);
	});

	it('should not dispatch addTodo when put invalid todo text', () => {
		var todoText = '';
		var spy = expect.createSpy();
		var addTodo = TestUtils.renderIntoDocument(<AddTodoForm dispatch={spy}/>);
		var $el = $(ReactDOM.findDOMNode(addTodo));

		addTodo.refs.addTodoFields.value = todoText;
		TestUtils.Simulate.submit($el.find('form')[0]);

		expect(spy).toNotHaveBeenCalled();
	});
});
