import React from 'react'
import ReactDOM from 'react-dom'
var {Provider} = require('react-redux');
import TestUtils from 'react-addons-test-utils'
import expect from 'expect'
import jquery from 'jquery'

var configureStore = require('configureStore');

import TodoApp from 'TodoApp'
import TodoList from 'TodoList'

describe('TODOAPP OPERATIONS', () => {
	it('TodoApp component should exist ', () => {
		expect(TodoApp).toExist();
	});

	it('TodoApp should render TodoList', () => {
		let store = configureStore.configure();
		let provider = TestUtils.renderIntoDocument(
			<Provider store={store}>
				<TodoApp />
			</Provider>
		);

		//fetch todoApp instance - store the first one 
		var todoApp = TestUtils.scryRenderedComponentsWithType(provider, TodoApp)[0]
		var todoList = TestUtils.scryRenderedComponentsWithType(todoApp, TodoList);

		//if 0 - that todoList never render
		expect(todoList.length).toEqual(1);
	});
});
