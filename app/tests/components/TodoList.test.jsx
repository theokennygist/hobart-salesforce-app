import React from 'react'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'
import expect from 'expect'
import $ from 'jquery'
var {Provider} = require('react-redux');

import ConnectedTodoList from 'TodoList'
import {TodoList} from 'TodoList'

import ConnectedTodoObj from 'TodoObj'
import {TodoObj} from 'TodoObj'
import {configure} from 'configureStore'

describe('TODOLIST OPERATIONS', () => {
	it('TodoList component should exist ', () => {
		expect(TodoList).toExist();
	});

	it('should render one Todo for each todo item', () => {
		
		//create todos dummy data
		var todos = [{
			id: 1,
			text: 'do something',
			completed: false,
			completedAt: undefined,
			createdAt: 500
		}, 
		{
			id: 2, 
			text: 'check email',
			completed: false,
			completedAt: undefined,
			createdAt: 500
		}];
		
		//set up initial state
		var store = configure({
			todos 
		})

		var provider = TestUtils.renderIntoDocument(
			<Provider store={store}>
				<ConnectedTodoList />
			</Provider>
			)

		
		let todoList = TestUtils.scryRenderedComponentsWithType(provider, ConnectedTodoList)[0];

		//store todosComponent that are found in todoList into new variable
		//how many todos component that are rendered inside todoList
		let todosComponents = TestUtils.scryRenderedComponentsWithType(todoList, ConnectedTodoObj)

		//check that todosComponent length the same with todos array above
		expect(todosComponents.length).toBe(todos.length);
		
	});

	it('should render an empty message if no todos', () => {
		
			var todos = [];
    		var todoList = TestUtils.renderIntoDocument(<TodoList todos={todos}/>);
    		var $el = $(ReactDOM.findDOMNode(todoList));

    		expect($el.find('.container__message').length).toBe(1);	
	});
});
