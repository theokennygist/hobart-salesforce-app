import React from 'react'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'
import expect from 'expect'
import $ from 'jquery'

import {TodoObj} from 'TodoObj'

describe('TODOOBJ OPERATIONS', () => {
	it('TodoObj component should exist ', () => {
		expect(TodoObj).toExist();
	});

	it('should dispatch TOGGLE_TODO action on click', () => {
		var todoData = {
			id: 199,
			text: 'write test',
			completed: true
		};
		var spy = expect.createSpy();
		var todo = TestUtils.renderIntoDocument(<TodoObj {...todoData} dispatch={spy} />)

		var $el = $(ReactDOM.findDOMNode(todo));

		TestUtils.Simulate.click($el[0]);

		expect(spy).toHaveBeenCalledWith({
			type: 'TOGGLE_TODO',
			id: todoData.id
		});
	});
});
