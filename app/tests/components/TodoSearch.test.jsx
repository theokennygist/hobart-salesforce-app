import React from 'react'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'
import expect from 'expect'
import $ from 'jquery'

import {TodoSearch} from 'TodoSearch'

describe('TODOSEARCH OPERATIONS', () => {
	
	it('TodoSearch component should exist', () => {
		expect(TodoSearch).toExist();
	});

	//test search textfield
	it('should dispatch setSearchText on input change', () => {
		var searchText = 'Dog'
		var spy = expect.createSpy();
		
		var action = {
			type: 'SET_SEARCH_TEXT',
			searchText
		};

		//onSearch - prop
		var todoSearch = TestUtils.renderIntoDocument(<TodoSearch dispatch={spy}/>)

		//change value in input text
		todoSearch.refs.searchText.value = searchText
		TestUtils.Simulate.change(todoSearch.refs.searchText) //test change event

		expect(spy).toHaveBeenCalledWith(action) //test checkbox, searchText
	});

	//test checkbox completed
	it('should dispatch Toggle_show_completed when checkbox is checked', () => {
		var spy = expect.createSpy();

		var action = {
			type: 'TOGGLE_SHOW_COMPLETED',
		};

		var todoSearch = TestUtils.renderIntoDocument(<TodoSearch dispatch={spy} />)

		//check the checkbox to be true
		todoSearch.refs.showCompleted.checked = true;
		//simulate the change 
		TestUtils.Simulate.change(todoSearch.refs.showCompleted)

		expect(spy).toHaveBeenCalledWith(action);
	})
});