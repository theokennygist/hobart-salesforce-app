import expect from 'expect'
import TodoApi from 'TodoApi'

describe('TODO API OPERATIONS', () => {
	//method called before others - clean up todos
	beforeEach(() => {
		localStorage.removeItem('todos');
	});

	it('TODO API should exists', () => {
		expect(TodoApi).toExist();
	});

	//SET TODOS 
	describe('SET TODOS SECTIONS', () => {
		
		//TEST with valid data
		it('should set valid todos array', () => {
			var todos = [{
				id: 23,
				text: 'Test files',
				completed: false
			}];

			TodoApi.setTodos(todos);

			var actualTodo = JSON.parse(localStorage.getItem('todos'));

			//work object & array toEqual
			expect(actualTodo).toEqual(todos)
		});
		//test with invalid data
		it('should not set todos with invalid todos array', () => {
			var badTodos = {a: 'b'};
			TodoApi.setTodos(badTodos);

			expect(localStorage.getItem('todos')).toBe(null);
		});
	});

	//GET TODOS
	describe('GET TODOS SECTIONS', () => {
		it('should return an empty array for bad localStorage data', () => {
			
			var actualTodos = TodoApi.getTodos();
			expect(actualTodos).toEqual([]);
		});

		it('should return todo if pass valid array in localStorage', () => {
			var todos = [{
				id: 23,
				text: 'Test files',
				completed: false
			}];

			localStorage.setItem('todos', JSON.stringify(todos));
			var actualTodos = TodoApi.getTodos();
			expect(actualTodos).toEqual(todos);
		})
	});

	describe('FILTER TODOS SECTIONS', () => {
		var todos = [{
			id: 1,
			text: 'some texts',
			completed: true
		},
		{
			id: 2,
			text: 'some other texts',
			completed: false
		}, 
		{
			id: 3,
			text: 'some last test',
			completed: true
		}];

		it('should return all items if showCompleted is true', () => {
			//todos - todos array above
			var filteredTodos = TodoApi.filterTodos(todos, true, '');

			//expect length to be 3 
			expect(filteredTodos.length).toBe(3);
		});

		it('should return all items if showCompleted is false', () => {
			var filteredTodos = TodoApi.filterTodos(todos, false, '');

			//expect length to be 1
			expect(filteredTodos.length).toBe(1);
		});

		it('should sort by completed status', () => {
			var filteredTodos = TodoApi.filterTodos(todos, true, '');

			expect(filteredTodos[0].completed).toBe(false);
		});

		it('should filter todos by searchText', () => {
			//todos - todos array above
			var filteredTodos = TodoApi.filterTodos(todos, true, 'some');

			//expect length to be 3 
			expect(filteredTodos.length).toBe(3);
		});

		it('should return all todos if searchText is empty', () => {
			//todos - todos array above
			var filteredTodos = TodoApi.filterTodos(todos, true, '');

			//expect length to be 3 
			expect(filteredTodos.length).toBe(3);
		});
	});
});