import expect from 'expect'

var actions = require('actions');

describe('ACTIONS OPERATIONS', () => {
	it('should search text in search text action', () => {
	    var actionObj = {
	      type: 'SET_SEARCH_TEXT',
	      searchText: 'Some search text'
	    };
    	var res = actions.setSearchText(actionObj.searchText);

    	expect(res).toEqual(actionObj);
  	});

  	it('should add todo inside todoList action', () => {
	    var actionObj = {
	      type: 'ADD_TODO',
	      text: 'exercise'
	    };

    	var res = actions.addTodo(actionObj.text);

    	expect(res).toEqual(actionObj);
  	});

    it('should retrieve todos in add todos action', () => {
        var todos = [{
          id: 111,
          text: 'Any',
          completed: false,
          completedAt: undefined,
          createdAt: 33000
        }];

        var action = {
          type: 'ADD_TODOS',
          todos
        }
        var result = actions.addTodos(todos);
        expect(result).toEqual(action);

    });

  	it('should update show completed value todo action', () => {
  		var actionObj = {
  			type: 'TOGGLE_SHOW_COMPLETED'
  		}

  		var res = actions.toggleShowCompleted();
  		expect(res).toEqual(actionObj);
  	});

  	it('should update toggle todo action', () => {
	    var action = {
	      type: 'TOGGLE_TODO',
	      id: '123'
	    };
	    var res = actions.toggleTodo(action.id);

	    expect(res).toEqual(action);
  	});
})
