module.exports = {
	setTodos: (todos) => {
		if(Array.isArray(todos)) {
			//convert array into string
			localStorage.setItem('todos', JSON.stringify(todos));
			return todos;
		}
	},
	getTodos: () => {
		var stringTodo = localStorage.getItem('todos');
		var todos = [];

		try {
			todos = JSON.parse(stringTodo);
		}
		catch(e) {

		}

		return Array.isArray(todos) ? todos : []
	},

	filterTodos: (todos, showCompleted, searchText) => {
		let filteredTodos = todos;

		//filter by showCompleted
		filteredTodos = filteredTodos.filter((todo) => {
			return !todo.completed || showCompleted;
		});

		//filter by searchText
		//filter - true -> stay in the array
		//filter - false -> remove from the array
		filteredTodos = filteredTodos.filter((todo) => {
			//indexOf - if text inside another text
			var text = todo.text.toLowerCase();
			return searchText.length === 0 || text.indexOf(searchText) > -1;
		});

		//sort todos with non-completed first
		// -1 a come before b
		// 1 b come before a
		// 0 a & b are equal
		filteredTodos.sort((a, b) => {
			if(!a.completed && b.completed) {
				return -1
			}
			else if(a.completed && !b.completed) {
				return 1 //a come after b
			}
			else {
				return 0; //a equal b so no needs to sort
			}
		});

		return filteredTodos
	}
};